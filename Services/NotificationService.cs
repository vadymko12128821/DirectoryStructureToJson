﻿using System.Windows;
using Extensibility;
using MessageBox = System.Windows.MessageBox;
using MessageBoxOptions = System.Windows.MessageBoxOptions;

namespace Services
{
    public class NotificationService : INotificationService
    {
        private const string SuccessfulConvertToJsonMessage =
            "My congratulation you are successful serialize your files!";

        
        public static void ShowSuccessulMessageBox(string message)
        {
            MessageBox.Show(message, "Congratulation", MessageBoxButton.OK, MessageBoxImage.Information,
                MessageBoxResult.OK, MessageBoxOptions.ServiceNotification);
        }

        public static void ShowErrorMessageBox(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error,
                MessageBoxResult.OK, MessageBoxOptions.ServiceNotification);
        }
        
        public void SuccessfulConvertToJson() => ShowSuccessulMessageBox(SuccessfulConvertToJsonMessage);

        public void ErrorConvert(string message) => ShowErrorMessageBox(message);
    }
}
