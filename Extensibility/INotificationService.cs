﻿namespace Extensibility
{
    public interface INotificationService
    {
        void SuccessfulConvertToJson();
        void ErrorConvert(string message);
    }
}