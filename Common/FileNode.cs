﻿using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class FileNode
    {
        [DataMember(Name = "Name", IsRequired = true)]
        public string Name { get; set; }

        [DataMember(Name = "Size", IsRequired = true)]
        public string FileSize { get; set; }

        [DataMember(Name = "Path", IsRequired = true)]
        public string Path { get; set; }
    }
}